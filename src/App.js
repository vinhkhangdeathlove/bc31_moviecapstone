import "./App.css";
import LoginPage from "./Page/LoginPage/LoginPage";
import RegisterPage from "./Page/LoginPage/RegisterPage";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import HomePage from "./Page/HomePage/HomePage";
import Layout from "./HOC/Layout";
import DetailPage from "./Page/DetailPage/DetailPage";
import SpinnerComponent from "./components/SpinnerComponent/SpinnerComponent";
import PurchasePage from "./Page/PurchasePage/PurchasePage";

function App() {
  return (
    <div className="container mx-auto bg__movie">
      <SpinnerComponent />
      <BrowserRouter>
        <Routes>
          {/* <Route path="/" element={<Layout Component={<HomePage />} />} /> */}
          <Route path="/" element={<Layout Component={HomePage} />} />
          <Route
            path="/detail/:id"
            element={<Layout Component={DetailPage} />}
          />
          <Route path="/login" element={<LoginPage />} />
          <Route path="/register" element={<RegisterPage />} />
          <Route
            path="/purchase/:param"
            element={<Layout Component={PurchasePage} />}
          />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
