import React from "react";
import HeaderTheme from "../components/HeaderTheme/HeaderTheme";
import FooterTheme from "../components/FooterTheme/FooterTheme";

export default function Layout({ Component }) {
  return (
    <div className="">
      <HeaderTheme />
      <Component />
      <FooterTheme />
    </div>
  );
}
