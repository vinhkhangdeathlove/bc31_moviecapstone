import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { movieService } from "../../services/movie.service";
import "./detail.scss";
import MovieDetail from "./MovieDetail/MovieDetail";
import SpinnerComponent from "../../components/SpinnerComponent/SpinnerComponent";
import TabMovie from "./MovieDetailTab/TabMovie";

export default function DetailPage() {
  let param = useParams();
  const [movieDetail, setMovieDetail] = useState({});
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    setIsLoading(true);
    setTimeout(() => {
      movieService
        .getDetailMovie(param.id)
        .then((res) => {
          setIsLoading(false);
          setMovieDetail(res.data.content);
        })
        .catch((err) => {
          console.log("err", err);
          setIsLoading(false);
        });
    }, 500);
  }, []);

  return (
    <div>
      {!isLoading ? <SpinnerComponent /> : ""}
      <div id="movieDetail" className="bg__movie px-[10px]">
        <div className="max-w-7xl container mx-auto">
          <MovieDetail movieDetail={movieDetail} />
          <TabMovie maPhim={param.id} />
        </div>
      </div>
    </div>
  );
}
