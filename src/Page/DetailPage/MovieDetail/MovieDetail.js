import { Progress } from "antd";
import "../detail.scss";

export default function MovieDetail({ movieDetail }) {
  return (
    <div className="movie__card pt-20 rounded">
      <div className="info__section">
        <div className="movie__header">
          <img
            src={movieDetail.hinhAnh}
            alt={movieDetail.tenPhim}
            className="locandina"
          />
          <h1 className="text-2xl">{movieDetail.tenPhim}</h1>
          <h4 className="my-2">2017, David Ayer</h4>
          <span>
            <Progress
              type="circle"
              width={70}
              strokeWidth={12}
              percent={movieDetail.danhGia * 10}
              format={(percent) => `${percent / 10}`}
              strokeColor={{
                "0%": "#ffd000",
                "50%": "#ffc300",
                "100%": "#ff9500",
              }}
              trailColor="#618985"
            />
          </span>
          <span className="minutes ml-3">117 min</span>
          <p className="ml-3">Action, Crime, Fantasy</p>
        </div>
        <div className="movie__desc">
          <p>{movieDetail.moTa}</p>
        </div>
        <div className="movie__social">
          <ul>
            <div>
              <li>
                <i className="fa fa-share-alt" />
              </li>
              <li>
                <i className="fa fa-heart" />
              </li>
              <li>
                <i className="fa fa-comment" />
              </li>
            </div>
          </ul>
        </div>
      </div>
      <div
        className="blur__back bright__back"
        style={{
          background: `url('${movieDetail.hinhAnh}')`,
        }}
      />
    </div>
  );
}
