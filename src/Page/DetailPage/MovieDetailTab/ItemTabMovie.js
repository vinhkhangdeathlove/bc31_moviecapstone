import moment from "moment";
import React from "react";
import { NavLink } from "react-router-dom";

export default function ItemTabMovie({ rapChieu }) {
  return (
    <div className="flex space-x-10 p-5 ">
      <div className="grid grid-cols-3 gap-4">
        {rapChieu.lichChieuPhim.slice(0, 8).map((lichChieu, index) => {
          return (
            <NavLink to={`/purchase/${lichChieu.maLichChieu}`} key={index}>
              <div
                className="
                bg-red-600 text-white p-2 rounded
              "
              >
                {moment(lichChieu.ngayChieuGioChieu).format("DD/MM/yyyy")}

                <span className="text-yellow-400 font-bold text-base ml-5">
                  {moment(lichChieu.ngayChieuGioChieu).format("hh:mm")}
                </span>
              </div>
            </NavLink>
          );
        })}
      </div>
    </div>
  );
}
