import React, { useEffect, useState } from "react";
import { Tabs } from "antd";
import { movieService } from "../../../services/movie.service";
import ItemTabMovie from "./ItemTabMovie";
import SpinnerComponent from "../../../components/SpinnerComponent/SpinnerComponent";
const { TabPane } = Tabs;
export default function TabMovie({ maPhim }) {
  const [dataRaw, setDataRaw] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    setIsLoading(true);
    movieService
      .getDetailTabMovie(maPhim)
      .then((res) => {
        setIsLoading(false);
        setDataRaw(res.data.content);
      })
      .catch((err) => {
        setIsLoading(false);
        console.log(err);
      });
  }, []);

  const renderContent = () => {
    return dataRaw.heThongRapChieu?.map((cumRap, index) => {
      return (
        <TabPane
          tab={<img className="w-16" src={cumRap.logo} alt="" />}
          key={index}
        >
          <Tabs style={{ height: 500 }} tabPosition="left" defaultActiveKey="1">
            {/* react spinner */}
            {cumRap.cumRapChieu?.map((rapChieu) => {
              return (
                <TabPane
                  tab={renderTenCumRap(rapChieu)}
                  key={rapChieu.maCumRap}
                >
                  <div
                    style={{ height: 500, overflowY: "scroll" }}
                    className="shadow"
                  >
                    <ItemTabMovie rapChieu={rapChieu} key={index} />
                  </div>
                </TabPane>
              );
            })}
          </Tabs>
        </TabPane>
      );
    });
  };

  const renderTenCumRap = (cumRapChieu) => {
    return (
      <div className="text-left w-60 ">
        <p className="text-green-700 truncate">{cumRapChieu.tenCumRap}</p>
        <p className=" text-black truncate">{cumRapChieu.diaChi}</p>
        <button className="text-red-600">[ Xem chi tiết ] </button>
      </div>
    );
  };
  return (
    <div className="bg-white container mx-auto my-20 p-10 rounded">
      {isLoading ? <SpinnerComponent /> : ""}
      <Tabs tabPosition="left" defaultActiveKey="1">
        {renderContent()}
      </Tabs>
    </div>
  );
}
