import React, { Component } from "react";
import Slider from "../../components/Slider/Slider";
import ListMovie from "./ListMovie";
import TabMovie from "./TabMovie";

export default class HomePage extends Component {
  render() {
    return (
      <div className="bg__movie space-y-20">
        <div className="w-full h-max">
          <Slider />
        </div>
        <div className="container mx-auto pb-8 max-w-7xl">
          <h3
            className="text-white text-center text-3xl font-semibold mb-10"
            id="list__movies"
          >
            Danh sách phim
          </h3>
          <ListMovie />
          <div className="px-[10px] my-20" id="showtimes">
            <h3 className="text-white text-center text-3xl font-semibold mb-10">
              Lịch chiếu phim
            </h3>
            <TabMovie />
          </div>
        </div>
      </div>
    );
  }
}
