import { Card, Carousel } from "antd";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import { getMovieListActionService } from "../../redux/actions/movieAction";
import "./movieItem.scss";

export default function ListMovie() {
  let dispatch = useDispatch();

  let { movieList } = useSelector((state) => {
    return state.movieReducer;
  });

  useEffect(() => {
    dispatch(getMovieListActionService());
  }, []);

  let renderMovieList = () => {
    return (
      <div className="carousel">
        <Carousel dots={true} slidesToShow={4} slidesToScroll={4}>
          {movieList
            .filter((movie) => {
              return movie.hinhAnh !== null;
            })
            .map((movie) => {
              return (
                <Card
                  hoverable
                  style={{
                    width: "100%",
                  }}
                  key={movie.maPhim}
                  cover={
                    <img alt="" src={movie.hinhAnh} className="movieImg" />
                  }
                >
                  <div className="movieTitle">
                    <span>{movie.danhGia}</span>
                    {movie.tenPhim}
                  </div>
                  <div className="movieDesc">
                    <h4>{movie.moTa}</h4>
                  </div>
                  <NavLink to={`detail/${movie.maPhim}`}>
                    <button className="w-full bg-red-600 text-white rounded text-xl font-medium py-3">
                      Đặt vé
                    </button>
                  </NavLink>
                </Card>
              );
            })}
        </Carousel>
      </div>
    );
  };

  return <div className="">{renderMovieList()}</div>;
}
