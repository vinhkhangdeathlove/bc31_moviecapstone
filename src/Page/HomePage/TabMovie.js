import React, { useEffect, useState } from "react";
import { Tabs } from "antd";
import { movieService } from "../../services/movie.service";
import ItemTabMovie from "./ItemTabMovie";
import SpinnerComponent from "../../components/SpinnerComponent/SpinnerComponent";
const { TabPane } = Tabs;
export default function TabMovie() {
  const [dataMovie, setDataMovie] = useState([]);

  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    setIsLoading(true);
    movieService
      .getMovieByTheater()
      .then((res) => {
        setIsLoading(false);
        setDataMovie(res.data.content);
      })
      .catch((err) => {
        setIsLoading(false);
        console.log(err);
      });
  }, []);

  const onChange = (key) => {
    console.log(key);
  };
  const renderContent = () => {
    return dataMovie.map((heThongRap, index) => {
      return (
        <TabPane
          tab={<img className="w-16" src={heThongRap.logo} alt="" />}
          key={index}
        >
          <Tabs
            style={{ height: 500 }}
            tabPosition="left"
            defaultActiveKey="1"
            onChange={onChange}
          >
            {/* react spinner */}
            {heThongRap.lstCumRap.map((cumRap) => {
              return (
                <TabPane tab={renderTenCumRap(cumRap)} key={cumRap.maCumRap}>
                  <div
                    style={{ height: 500, overflowY: "scroll" }}
                    className="shadow"
                  >
                    {cumRap.danhSachPhim.map((phim) => {
                      return <ItemTabMovie phim={phim} key={phim.maPhim} />;
                    })}
                  </div>
                </TabPane>
              );
            })}
          </Tabs>
        </TabPane>
      );
    });
  };

  const renderTenCumRap = (cumRap) => {
    return (
      <div className="text-left w-60 ">
        <p className="text-green-700 truncate">{cumRap.tenCumRap}</p>
        <p className=" text-black truncate">{cumRap.diaChi}</p>
        <button className="text-red-600">[ Xem chi tiết ] </button>
      </div>
    );
  };
  return (
    <div className="bg-white container mx-auto p-10 rounded">
      {isLoading ? <SpinnerComponent /> : ""}
      <Tabs tabPosition="left" defaultActiveKey="1" onChange={onChange}>
        {renderContent()}
      </Tabs>
    </div>
  );
}
