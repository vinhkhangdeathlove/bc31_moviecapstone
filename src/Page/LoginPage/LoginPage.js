import React from "react";
import { Form, Input, message } from "antd";
import { userService } from "../../services/user.service";
import { localStorageServ } from "../../services/localStorageService";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { loginAction } from "../../redux/actions/userAction";
import LoginAnimate from "./LoginAnimate";

export default function LoginPage() {
  let dispatch = useDispatch();
  let history = useNavigate();
  const onFinish = (values) => {
    console.log("Success:", values);
    // history(-1);
    userService
      .postLogin(values)
      .then((res) => {
        message.success("Đăng nhập thành công");
        // console.log(res);
        dispatch(loginAction(res.data.content));
        localStorageServ.user.set(res.data.content);
        console.log(localStorageServ.user.get());
        // window.location.href = "/";
        setTimeout(() => {
          // chuyển trang
          history("/");
        }, 1000);
      })
      .catch((err) => {
        message.error(err?.response?.data.content);
      });
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  return (
    <div className="bg__movie h-screen w-screen pt-24">
      <div className="bg-white mx-auto rounded-xl flex w-1/2 p-10">
        <div className="w-1/2 text-center">
          <h3 className="text-2xl font-bold">
            Đăng nhập <span className="text-red-600">CyberMovie</span>
          </h3>
          <div className="flex justify-center">
            <div className="w-2/3">
              <LoginAnimate />
            </div>
          </div>
        </div>
        <div className="w-1/2">
          <Form
            name="basic"
            layout="vertical"
            labelCol={{
              span: 8,
            }}
            wrapperCol={{
              span: 24,
            }}
            initialValues={{
              remember: true,
            }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
          >
            <Form.Item
              label={<p className="font-medium text-blue-500">Tài khoản</p>}
              name="taiKhoan"
              rules={[
                {
                  required: true,
                  message: "Vui lòng nhập tài khoản",
                },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              label={<p className="font-medium text-blue-500">Mật khẩu</p>}
              name="matKhau"
              rules={[
                {
                  required: true,
                  message: "Vui lòng nhập mật khẩu",
                },
              ]}
            >
              <Input.Password />
            </Form.Item>

            <div className="text-center">
              <button className="py-2 px-5 bg-blue-600 text-white font-semibold uppercase text-sm rounded hover:bg-blue-700 transition duration-150 ease-in-out">
                Đăng nhập
              </button>
              <p className="text-sm font-semibold mt-2 pt-1 mb-0">
                Bạn mới biết đến CyberMovie?
                <a
                  href="/register"
                  className="text-red-600 hover:text-red-700 ml-2 transition duration-200 ease-in-out"
                >
                  Đăng ký
                </a>
              </p>
            </div>
          </Form>
        </div>
      </div>
    </div>
  );
}
