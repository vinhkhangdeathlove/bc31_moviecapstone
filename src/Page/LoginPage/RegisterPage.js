import React from "react";
import { Form, Input, message } from "antd";
import { userService } from "../../services/user.service";
import { useNavigate } from "react-router-dom";
export default function LoginPage() {
  let history = useNavigate();
  const onFinish = (values) => {
    const cloneValues = {
      ...values,
      maNhom: "GP06",
    };
    userService
      .postRegister(cloneValues)
      .then((res) => {
        message.success("Đăng ký thành công");
        setTimeout(() => {
          // chuyển trang
          history("/login");
        }, 1000);
      })
      .catch((err) => {
        message.error(err?.response?.data.content);
      });
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  return (
    <div className="bg__movie h-screen w-screen pt-24">
      <div className="bg-white mx-auto rounded-xl w-1/2 p-10">
        <h3 className="text-2xl font-bold text-center">
          Đăng ký <span className="text-red-600">CyberMovie</span>
        </h3>
        <Form
          name="basic"
          layout="vertical"
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
          className="flex mt-5"
        >
          <div className="w-1/2 mr-5">
            <Form.Item
              label={<p className="font-medium text-blue-500">Tài khoản</p>}
              name="taiKhoan"
              rules={[
                {
                  required: true,
                  message: "Vui lòng nhập tài khoản",
                },
              ]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              label={<p className="font-medium text-blue-500">Họ tên</p>}
              name="hoTen"
              rules={[
                {
                  required: true,
                  message: "Vui lòng nhập họ tên",
                },
              ]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              label={<p className="font-medium text-gray-500">Mật khẩu</p>}
              name="matKhau"
              rules={[
                {
                  required: true,
                  message: "Vui lòng nhập mật khẩu",
                },
              ]}
            >
              <Input.Password />
            </Form.Item>
          </div>
          <div className="w-1/2 ml-5">
            <Form.Item
              label={<p className="font-medium text-blue-500">Email</p>}
              name="email"
              rules={[
                {
                  required: true,
                  message: "Vui lòng nhập email",
                },
              ]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              label={<p className="font-medium text-blue-500">Số điện thoại</p>}
              name="soDt"
              rules={[
                {
                  required: true,
                  message: "Vui lòng nhập tài khoản",
                },
              ]}
            >
              <Input />
            </Form.Item>
            <div className="text-center">
              <button className="py-2 px-5 bg-blue-600 text-white font-semibold uppercase text-sm rounded hover:bg-blue-700 transition duration-150 ease-in-out">
                Đăng ký
              </button>
              <p className="text-sm font-semibold mt-2 pt-1 mb-0">
                Đã có tài khoản
                <a
                  href="/login"
                  className="text-red-600 hover:text-red-700 ml-2 transition duration-200 ease-in-out"
                >
                  Đăng nhập
                </a>
              </p>
            </div>
          </div>
        </Form>
      </div>
    </div>
  );
}
