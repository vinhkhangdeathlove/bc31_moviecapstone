import React from "react";
import { useSelector } from "react-redux";
// import { delSeatFromCart } from "../../redux/actions/seatAction";
import { movieService } from "../../services/movie.service";

export default function BookingDetails({ thongTinPhim }) {
  let seatCart = useSelector((state) => state.seatReducer.seatCart);
  // let dispatch = useDispatch();

  const thanhToan = () => {
    let values = {
      maLichChieu: thongTinPhim.maLichChieu,
      danhSachVe: seatCart.map((chair) => {
        return {
          maGhe: chair.maGhe,
          giaVe: chair.giaVe,
        };
      }),
    };

    movieService
      .postBookingTicket(values)
      .then((res) => {
        // dispatch(delSeatFromCart());
        window.location.reload();
      })
      .catch((err) => {
        console.log(err);
      });
  };
  return (
    <div className="mx-auto w-1/3">
      <div className="text-left bg-blue-100 rounded mb-4">
        <div className="p-4">
          <h4 className="font-bold text-black text-center uppercase">
            Thông tin phim
          </h4>
          <div className="py-2 border-b-[1px] border-gray-400">
            <b className="mr-4 ">Tên phim:</b>
            <b className="text-red-500">{thongTinPhim.tenPhim}</b>
          </div>
          <div className="py-2 border-b-[1px] border-gray-400">
            <b className="mr-4">Tên rạp:</b>
            {thongTinPhim.tenCumRap} | {thongTinPhim.tenRap}
          </div>
          <div className="py-2 border-b-[1px] border-gray-400">
            <b className="mr-4">Giờ chiếu:</b>
            {thongTinPhim.gioChieu}
          </div>
          <div className="py-2 border-b-[1px] border-gray-400">
            <b className="mr-4">Ngày chiếu:</b>
            {thongTinPhim.ngayChieu}
          </div>
          <div className="py-2 border-b-[1px] border-gray-400">
            <b className="mr-4">Ghế:</b>
            {seatCart.map((item, i) => {
              return item.loaiGhe === "Vip" ? (
                <span>
                  <b className="text-yellow-500">{item.tenGhe}</b>
                  {", "}
                </span>
              ) : (
                <b>{item.tenGhe + ", "}</b>
              );
            })}
            <i className="block text-blue-700 font-bold text-sm">
              * Ghế thường (75000k), Ghế VIP (90000k)
            </i>
          </div>
          <div className="py-2 border-b-[1px] border-gray-400">
            <b className="mr-4">Tổng:</b>
            <span className="text-red-500 text-xl">
              {seatCart.length === 0
                ? 0
                : seatCart
                    .map((item) => {
                      return item.giaVe;
                    })
                    .reduce((tong, gia) => {
                      return tong + gia;
                    })}
            </span>
          </div>
        </div>
      </div>
      <button
        className="flex justify-center bg-red-300 mx-auto px-5 py-2 uppercase font-semibold text-lg text-red-700 rounded-md hover:bg-red-800 hover:text-white transition-all"
        onClick={() => {
          thanhToan();
        }}
      >
        Thanh toán
      </button>
    </div>
  );
}
