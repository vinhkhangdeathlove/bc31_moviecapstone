import React from "react";
import SeatCondition from "../../components/Seat/SeatCondition";
import HangGhe from "./HangGhe";

export default function DanhSachGhe({ danhSachGhe }) {
  return (
    <div className="seatCharts-container mx-auto">
      <div className="screen">SCREEN</div>
      {danhSachGhe.map((hangGhe, index) => {
        return <HangGhe hangGhe={hangGhe} key={index} />;
      })}
      <SeatCondition />
    </div>
  );
}
