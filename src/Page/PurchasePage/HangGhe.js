import React from "react";
import Ghe from "./Ghe";

export default function HangGhe({ hangGhe }) {
  return (
    <div className="seatCharts-row flex justify-center mx-auto">
      {hangGhe.map((ghe, i) => {
        return <Ghe key={i} ghe={ghe} />;
      })}
    </div>
  );
}
