import React, { useEffect, useState } from "react";
import "./purchasePage.scss";
import { Navigate, useParams } from "react-router-dom";
import { movieService } from "./../../services/movie.service";
import DanhSachGhe from "./DanhSachGhe";
import BookingDetails from "./BookingDetails";
import _ from "lodash";
import SpinnerComponent from "./../../components/SpinnerComponent/SpinnerComponent";
import { message } from "antd";
import { useSelector } from "react-redux";

export default function PurchasePage() {
  let { param } = useParams();
  const [danhSachGhe, setDanhSachGhe] = useState([]);
  const [thongTinPhim, setThongTinPhim] = useState({});
  const [isLoading, setIsLoading] = useState(false);
  let userInfor = useSelector((state) => {
    return state.userReducer.userInfor;
  });

  useEffect(() => {
    setIsLoading(true);
    movieService
      .getSeatByMovie(param)
      .then((res) => {
        let result = _.chunk(res.data.content.danhSachGhe, 16);
        setDanhSachGhe(result);
        setThongTinPhim(res.data.content.thongTinPhim);
        setIsLoading(false);
      })
      .catch((err) => {
        console.log("err", err);
        setIsLoading(false);
      });
  }, []);

  return (
    <div>
      {isLoading ? <SpinnerComponent /> : ""}
      {userInfor ? (
        <div className="purchase_content">
          <h3>{thongTinPhim.tenPhim}</h3>
          <p className="text-center">
            <span>{thongTinPhim.tenRap}</span> -{" "}
            <span>Giờ chiếu {thongTinPhim.gioChieu}</span>
          </p>
          <div className="main">
            <h2>Multiplex Theatre Showing Screen 1</h2>
            <div className="flex">
              <DanhSachGhe danhSachGhe={danhSachGhe} />
              <BookingDetails thongTinPhim={thongTinPhim} />
            </div>
          </div>
        </div>
      ) : (
        <div>
          {message.error("Bạn phải đăng nhập trước khi tiến hành mua vé")}
          <Navigate to={"/login"} />;
        </div>
      )}
    </div>
  );
}
