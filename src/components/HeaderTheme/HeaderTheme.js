import React from "react";
import UserNav from "./UserNav";
import { Link } from "react-router-dom";
import "./headerTheme.css";

export default function HeaderTheme() {
  return (
    <div className="bg__movie fixed z-10 left-0 right-0">
      <div className="h-20 flex items-center justify-between shadow-lg container mx-auto max-w-7xl px-[10px]">
        <Link to="/">
          <div className="logo"></div>
        </Link>
        <div className="text-white flex space-x-16 text-xl font-semibold">
          <a href="#list__movies">Danh sách phim</a>
          <a href="#showtimes">Đặt vé</a>
          <a href="#contact">Liên hệ</a>
        </div>
        <UserNav />
      </div>
    </div>
  );
}
