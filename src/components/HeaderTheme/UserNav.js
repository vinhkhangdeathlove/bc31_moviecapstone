import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { loginAction } from "../../redux/actions/userAction";
import { localStorageServ } from "../../services/localStorageService";

export default function UserNav() {
  let dispatch = useDispatch();
  let userInfor = useSelector((state) => {
    return state.userReducer.userInfor;
  });

  const renderContent = () => {
    if (userInfor) {
      return (
        <div className="flex space-x-5 items-center">
          <p className="text-yellow-500 font-semibold text-lg">
            {userInfor.hoTen}
          </p>
          <button
            onClick={handleLogout}
            className="bg-red-500 text-white font-semibold text-lg rounded px-5 py-3"
          >
            Đăng xuất
          </button>
        </div>
      );
    } else {
      return (
        <div className="flex space-x-5 items-center">
          <button
            onClick={() => {
              window.location.href = "/login";
            }}
            className="bg-blue-500 text-white font-semibold text-lg rounded px-5 py-3"
          >
            Đăng nhập
          </button>
          <button
            onClick={() => {
              window.location.href = "/register";
            }}
            className="bg-orange-500 text-white font-semibold text-lg rounded px-5 py-3"
          >
            Đăng kí
          </button>
        </div>
      );
    }
  };
  const handleLogout = () => {
    localStorageServ.user.remove();
    dispatch(loginAction(null));
  };
  return <div>{renderContent()}</div>;
}
