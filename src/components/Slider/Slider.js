import React, { useEffect, useState } from "react";
import "./slider.css";
import { Carousel } from "antd";
import { movieService } from "../../services/movie.service";
import {
  LeftOutlined,
  PlayCircleOutlined,
  RightOutlined,
} from "@ant-design/icons";
import Fancybox from "../Fancybox/Fancybox";

export default function Slider() {
  const [listImg, setListImg] = useState([]);
  const linkYoutube = [
    "https://youtu.be/uqJ9u7GSaYM",
    "https://youtu.be/kBY2k3G6LsM",
    "https://youtu.be/_rUC3-pNLyc",
  ];

  useEffect(() => {
    movieService
      .getBannerMovie()
      .then((res) => {
        setListImg(res.data.content);
      })
      .catch((err) => {
        console.log("err", err);
      });
  }, []);

  return (
    <Carousel
      dots={true}
      arrows
      prevArrow={<LeftOutlined />}
      nextArrow={<RightOutlined />}
    >
      {listImg.slice(0, 3).map((item) => {
        return (
          <div className="slider relative h-screen" key={item.maBanner}>
            <div className="w-full">
              <div className="img">
                <img src={item.hinhAnh} alt="" className="hinhAnh " />
                <Fancybox>
                  <div className="overlay">
                    <button
                      className="p-5 bg-transparent rounded-full"
                      data-fancybox="gallery"
                      data-src={linkYoutube[item.maBanner - 1]}
                    >
                      <PlayCircleOutlined className="text-6xl text-white" />
                    </button>
                  </div>
                </Fancybox>
              </div>
            </div>
          </div>
        );
      })}
    </Carousel>
  );
}
