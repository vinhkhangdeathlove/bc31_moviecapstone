import { combineReducers } from "redux";
import { movieReducer } from "./movieReducer";
import { spinnerReducer } from "./spinnerReducer";
import { userReducer } from "./userReducer";
import { seatReducer } from "./seatReducer";

export let rootReducer = combineReducers({
  userReducer,
  movieReducer,
  spinnerReducer,
  seatReducer,
});
