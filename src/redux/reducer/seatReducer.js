import { ADD_SEAT_ITEM, DEL_SEAT_ITEM } from "../constants/seatConstant";

const initialState = {
  seatCart: [],
  seatCondition: "available",
};

export const seatReducer = (state = initialState, { type, payload, maGhe }) => {
  switch (type) {
    case ADD_SEAT_ITEM: {
      let cloneSeatCart = [...state.seatCart];
      let index = cloneSeatCart.findIndex((item) => {
        return item.maGhe === maGhe;
      });
      if (index !== -1) {
        cloneSeatCart.splice(index, 1);
      } else {
        if (!payload.daDat) {
          cloneSeatCart.push(payload);
          payload.daDat = true;
        } else payload.daDat = false;
      }

      state.seatCart = cloneSeatCart;
      return { ...state };
    }
    case DEL_SEAT_ITEM: {
      let cloneSeatCart = [];
      state.seatCart = cloneSeatCart;
      return { ...state };
    }
    default:
      return state;
  }
};
