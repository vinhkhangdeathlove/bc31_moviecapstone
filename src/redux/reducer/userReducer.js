import { localStorageServ } from "../../services/localStorageService";

let initiState = {
  userInfor: localStorageServ.user.get(),
};

export let userReducer = (state = initiState, action) => {
  switch (action.type) {
    case "LOGIN": {
      state.userInfor = action.payload;
      return { ...state };
    }
    default:
      return state;
  }
};
