import { https } from "./configURL";

export let userService = {
  postLogin: (loginData) => {
    return https.post("/api/QuanLyNguoiDung/DangNhap", loginData);
  },
  postRegister: (values) => {
    return https.post("/api/QuanLyNguoiDung/DangKy", values);
  },
};
