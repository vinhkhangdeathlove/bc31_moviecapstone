/** @type {import('tailwindcss').Config} */
module.exports = {
  mode: "jit",
  important: true,
  content: ["./src/**/*.{js,jsx,ts,tsx}", "./assets/**.{png,jpg}"],
  theme: {
    extend: {},
  },
  plugins: [],
};
